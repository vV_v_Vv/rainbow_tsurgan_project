package ru.itcube64;

import javax.swing.*;
import java.awt.*;

public class BubbleGame extends JFrame {
    public static JLabel score;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Bubble game");
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(830, 650);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);

        BorderLayout borderLayout = new BorderLayout();
        frame.getContentPane().setLayout(borderLayout);

        BubblePanel panelCenter = new BubblePanel();
        frame.getContentPane().add("Center", panelCenter);


        JPanel panelEast = new JPanel();
        panelEast.setBackground(Color.LIGHT_GRAY);
        GridLayout gridLayout = new GridLayout(10, 2);
        panelEast.setLayout(gridLayout);

        JLabel label = new JLabel("      Score      ");
        label.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 26));
        label.setHorizontalTextPosition(SwingConstants.CENTER);
        panelEast.add(label);

        score = new JLabel("0");
        score.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 26));
        score.setHorizontalAlignment(SwingConstants.CENTER);
        score.setHorizontalTextPosition(SwingConstants.CENTER);
        panelEast.add(score);

        frame.getContentPane().add("East", panelEast);
        //frame.pack();
        frame.setVisible(true);

        panelCenter.setFrame(frame);
        panelCenter.startGame();
    }
}



